@TestOn('browser')
library util_test;

import 'package:over_react/over_react.dart';
import 'package:react/react_client.dart';
import 'package:test/test.dart';

import './header/header_test.dart' as header_test;
import '../web/src/test_components/menuItem_test.dart' as something_test;

void main(){
  setClientConfiguration();

  header_test.main();
  something_test.main();

}