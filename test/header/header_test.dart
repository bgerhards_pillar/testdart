import 'dart:html';

import 'package:over_react/over_react.dart';
import 'package:test/test.dart';

void main() {
  group("String", () {
    test("A should = A", () {
      var a = 'A';
      expect(a, equals('A'));
    });
  });
}