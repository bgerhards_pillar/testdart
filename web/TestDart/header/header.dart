part of dart.test.app;

@Factory()
UiFactory<HeaderProps> Header;

@Props()
class HeaderProps extends UiProps {
//  List<MenuItm> menuItems;
}

@State()
class HeaderState extends UiState {
  dynamic menuItems;
  int count;
}

@Component()
class HeaderComponent<T extends HeaderProps, S extends HeaderState>
    extends UiStatefulComponent<T, S> {

  @override
  Map getDefaultProps() =>
      (newProps()
//    ..menuItems = [new MenuItm('0', 'http://www.google.com', 'Google')]
      );

  @override
  Map getInitialState() =>
      (newState()
        ..menuItems = [
          new MenuItm('0', '#', 'Google'),
          new MenuItm('1', '#', 'Pillar Technology')]
        ..count = 0
      );

  @override
  render() {
    return renderHeader(props.children);
  }

  increaseCount(value) {
    setState(newState()
      ..count = state.count + 1
    );
    print(state.count);
    print(value);
    return false;
  }

  ReactElement renderHeader(dynamic children) {
    return (Dom.div()
      ..className = 'row')
      (

        (Dom.div()
          ..className = "col-md-8")
          (

            (Dom.ul()
              ..className = "list-group")
              (

                this.state.menuItems.map((menuItem) =>
                (
                    (MenuItem()
                      ..href = menuItem.href
                      ..label = menuItem.label
                      ..key = menuItem.id
                      ..className = 'list-group-item'
                      ..onclick = this.increaseCount
                    )()
                )
                )
            )
        ), (Dom.div()
      ..className = 'row'
    )((Dom.span()(
        state.count
    )))
    );
  }
}