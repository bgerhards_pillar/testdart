part of dart.test.app;

class MenuItm  {
  dynamic id;
  String href;
  String label;

  MenuItm(id, href, label){
    this.id = id;
    this.href = href;
    this.label = label;
  }
}