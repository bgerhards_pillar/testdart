part of dart.test.app_components;

@Factory()
UiFactory<MenuItemProps> MenuItem;

@Props()
class MenuItemProps extends UiProps {
  String label;
  String href;
  String className;
  dynamic onclick;
}

@State()
class MenuItemState extends UiState {
  int count;
}

@Component()
class MenuItemComponent<T extends MenuItemProps, S extends MenuItemState>
    extends UiStatefulComponent<T, S> {
  @override
  Map getDefaultProps() =>
      (newProps()
        ..label = ''
        ..href = ''
        ..key = ''
        ..className = ''
        ..onclick = (a) => print(a)
      );

  Map getInitialState() =>
      (newState()
        ..count = 0
      );

  void _increment() =>
      setState(newState()
        ..count = state.count + 1
      );

  void _handleClick(e) {
    _increment();
    props.onclick(this.props.label);
  }

  @override
  render() {
    return renderMenuItem(props.children);
  }

  ReactElement renderMenuItem(dynamic children) {

    return (Dom.li()
      ..className = props.className)
      (
        (Dom.a()
          ..addProps(copyUnconsumedDomProps())
          ..href = props.href
          ..onClick = _handleClick
          ..key = props.key
        )('${props.label} ${state.count}'));
  }
}
