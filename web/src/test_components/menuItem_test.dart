import 'package:over_react/over_react.dart';
import 'package:test/test.dart';
import 'package:over_react_test/over_react_test.dart';
import '../test_components.dart';


mockFunction(input) {
  print(input);
}

void main() {
//  group('Common Component Tests', () {
//    test("Common Component Test", () {
//      commonComponentTests((MenuItem));
//    });
//  });

  group('Figuring out this factory stuff...', () {
    test("Test Menu Item Factory Constructor", () {
      var renderedInstance = render(MenuItem()());

      MenuItemComponent menuItemComponent = getDartComponent(renderedInstance);
      expect(menuItemComponent.state.count, equals(0));
    });

    test("Test Menu Item Factory Constructor Validate Props", () {
      var renderedInstance = render((MenuItem()
        ..label = 'label test'
        ..href = 'http://www.google.com'
        ..className = 'row'
        ..onclick = (a) => print(a))('Hello world'));

      MenuItemComponent menuItemComponent = getDartComponent(renderedInstance);
      expect(menuItemComponent.props.length, equals(5));
      expect(menuItemComponent.props.label, equals('label test'));
      expect(menuItemComponent.props.href, equals('http://www.google.com'));
      expect(menuItemComponent.props.className, equals('row'));

      expect(menuItemComponent.props.children, equals(['Hello world']));
    });


    test("Check Instance", () {
      var renderedInstance = render((MenuItem()
        ..label = 'label test'
        ..href = 'http://www.google.com'
        ..className = 'row'
        ..onclick = (a) => print(a))('Hello world'));

      MenuItemComponent menuItemComponent = getDartComponent(renderedInstance);
      var nodeMenuItem = findDomNode(renderedInstance);
      print(nodeMenuItem);
    });
  });
}