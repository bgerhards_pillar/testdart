import 'dart:html';

import 'package:react/react_dom.dart' as react_dom;
import 'package:react/react_client.dart' show setClientConfiguration;
import './TestDart/TestDart.dart';
import './TestDart/constants.dart';

void main() {
  setClientConfiguration();

  react_dom.render(
  Header()(), querySelector('$demoMountNodeSelectorPrefix-header'));
}
